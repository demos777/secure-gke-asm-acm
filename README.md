# Improve Security Posture in GKE Environment with ACM and ASM

[[_TOC_]]

## Prerequisites

Before you begin, ensure you have the following requirements met:

- This guide has only been tested in [Google Cloud Shell](https://cloud.google.com/shell/docs/features).
- This guide assumes you have `owner` IAM permissions on the Google project. In production, you do not require `owner` permission. Please check [ASM documentation](https://cloud.google.com/service-mesh/v1.10/docs/scripted-install/gke-project-cluster-setup#setting_up_your_project) for exact IAM requirements.

## Objective

- **Demo Setup**

  - Enable required APIs.
  - Create two GKE clusters and register to Fleet.
  - Install ASM and ACM with Policy Controller and constraint templates.
  - Deploy Bank of Anthos application to the GKE clusters.

- **Demo**
  - Achieve a greater security posture in GKE by enabling policy constraints.
  - Configure Policy Controller constraints on the cluster and view violations.
  - The following built-in constraints are deployed:
    - _Strict mTLS_: Achieve end-to-end encryption between all services in your cluster by requiring mesh wide strict mTLS using ASM.
    - _Destination Rule mTLS_: Achieve enforcement of per-Service encryption by enforcing ASM Destination Rules to have have STRICT mTLS configured.
    - _SourceNotAllAuthz_: Achieve granular access policies to every Service by requiring ASM Authorization Policies to not have wildcards as principals.
    - _K8sRequireNamespaceNetworkPolicies_: Achieve granular traffic control between namespaces by enforcing that all namespaces must have (one or more) network policies defined.
  - In addition to the built-in constraint templates, you can also create custom constraint templates and constraints. Custom constraint templates can be used to enforce your organization specific policies.
    - _K8sRequireNamespaceNetworkPolicies_: Achieve granular access control between Services by enforcing that every namespace has (one or more) AuthorizationPolicy resources defined.
  - Resolve all the constraint violations by creating the required resources on the GKE clusters.

## Demo Setup

The following steps are required for demo setup.

### Setting up your environment

To set up your environment, follow these steps.

1.  In the Google Cloud Console, activate Cloud Shell.

    [ACTIVATE CLOUD SHELL](https://console.cloud.google.com/?cloudshell=true&_ga=2.89525019.1226080234.1626725693-1104188948.1619545283)

At the bottom of the Cloud Console page, a [Cloud Shell](https://cloud.google.com/shell/docs/features) session starts and displays a command-line prompt. Cloud Shell is a shell environment with the Cloud SDK and the [gcloud command-line tool](https://cloud.google.com/sdk/gcloud) already installed, and with values already set for your current project. It can take a few seconds for the session to initialize.

1.  Create a `WORKDIR` to store all associated files for this tutorial. You can then delete the folder when you're done.

    ```bash
    mkdir -p secure-gke && cd secure-gke && export WORKDIR=$(pwd)
    ```

1.  Configure your Google Cloud project.

    ```bash
    gcloud config set project PROJECT_ID
    ```

1.  Define variables used in this workshop. You can create a `vars.sh` in your `WORKDIR` folder so that you can always source them in case you lose access to Cloud Shell session.

    ```bash
    cat <<EOF > ${WORKDIR}/vars.sh
    export WORKDIR=${WORKDIR}
    export PROJECT_ID=$(gcloud info --format='value(config.project)')
    export GCLOUD_USER=$(gcloud info --format='value(config.account)')
    export WORKLOAD_POOL=$(gcloud info --format='value(config.project)').svc.id.goog
    export MESH_ID="proj-$(gcloud projects describe `gcloud info --format='value(config.project)'` --format='value(projectNumber)')"
    export CLUSTER_1=gke-west2-a
    export CLUSTER_1_ZONE=us-west2-a
    export CLUSTER_2=gke-central1-a
    export CLUSTER_2_ZONE=us-central1-a
    export ASM_CHANNEL=regular
    export ASM_LABEL=asm-managed
    export KUBECONFIG=${WORKDIR}/asm-kubeconfig
    export ACM_REPO=acm-repo
    export GSA_READER=asm-reader-sa
    EOF

    source ${WORKDIR}/vars.sh
    ```

1.  Enable the required APIs in your project.

    ```bash
    gcloud services enable \
    --project=${PROJECT_ID} \
    anthos.googleapis.com \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    cloudtrace.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    iamcredentials.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    multiclusteringress.googleapis.com \
    multiclusterservicediscovery.googleapis.com \
    stackdriver.googleapis.com \
    sourcerepo.googleapis.com \
    cloudresourcemanager.googleapis.com
    ```

1.  Clone this repo.

    ```bash
    git clone https://gitlab.com/demos777/secure-gke-asm-acm.git ${WORKDIR}/secure-gke-asm-acm
    cd ${WORKDIR}/secure-gke-asm-acm
    ```

1.  Create two GKE clusters.

    ```bash
    gcloud container clusters create ${CLUSTER_1} \
      --project ${PROJECT_ID} \
      --zone=${CLUSTER_1_ZONE} \
      --machine-type "e2-standard-4" \
      --num-nodes "4" --min-nodes "4" --max-nodes "6" \
      --monitoring=SYSTEM \
      --logging=SYSTEM,WORKLOAD \
      --enable-ip-alias \
      --enable-autoscaling \
      --workload-pool=${WORKLOAD_POOL} \
      --verbosity=none \
      --labels=mesh_id=${MESH_ID} --async

      gcloud container clusters create ${CLUSTER_2} \
      --project ${PROJECT_ID} \
      --zone=${CLUSTER_2_ZONE} \
      --machine-type "e2-standard-4" \
      --num-nodes "4" --min-nodes "4" --max-nodes "6" \
      --monitoring=SYSTEM \
      --logging=SYSTEM,WORKLOAD \
      --enable-ip-alias \
      --enable-autoscaling \
      --workload-pool=${WORKLOAD_POOL} \
      --verbosity=none \
      --labels=mesh_id=${MESH_ID}
    ```

1.  Get clusters credentials.

    ```bash
    touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
    gcloud container clusters get-credentials ${CLUSTER_1} --zone ${CLUSTER_1_ZONE}
    gcloud container clusters get-credentials ${CLUSTER_2} --zone ${CLUSTER_2_ZONE}

    kubectl config rename-context gke_${PROJECT_ID}_${CLUSTER_1_ZONE}_${CLUSTER_1} ${CLUSTER_1}
    kubectl config rename-context gke_${PROJECT_ID}_${CLUSTER_2_ZONE}_${CLUSTER_2} ${CLUSTER_2}

    kubectl config get-contexts
    ```

    The output is similar to the following:

    ```
    CURRENT   NAME             CLUSTER                                       AUTHINFO                                      NAMESPACE
    *         gke-central1-a   gke_securegke1_us-central1-a_gke-central1-a   gke_securegke1_us-central1-a_gke-central1-a
              gke-west2-a      gke_securegke1_us-west2-a_gke-west2-a         gke_securegke1_us-west2-a_gke-west2-a
    ```

### Registering GKE clusters to Fleet

1.  Register GKE clusters to fleet.

    ```bash
    # Cluster_1
    gcloud container hub memberships register ${CLUSTER_1} \
    --project=${PROJECT_ID} \
    --gke-cluster=${CLUSTER_1_ZONE}/${CLUSTER_1} \
    --enable-workload-identity

    # Cluster_2
    gcloud container hub memberships register ${CLUSTER_2} \
    --project=${PROJECT_ID} \
    --gke-cluster=${CLUSTER_2_ZONE}/${CLUSTER_2} \
    --enable-workload-identity
    ```

    The output is similar to the following:

    ```
    Finished registering the cluster [gke-west2-a] with the Fleet.
    Finished registering the cluster [gke-central1-a] with the Fleet.
    ```

1.  Enable the mesh and config management features in the project. Wait for the feature to become available. This could take a few minutes.

    ```bash
    gcloud beta container hub mesh enable --project=${PROJECT_ID}
    gcloud beta container hub config-management enable --project=${PROJECT_ID}
    gcloud beta container hub mesh describe
    ```

    The output is similar to the following:

    ```
    membershipStates:
      projects/831028511762/locations/global/memberships/gke-central1-a:
        state:
          code: OK
          description: Please see https://cloud.google.com/service-mesh/docs/install for
            instructions to onboard to Anthos Service Mesh.
          updateTime: '2022-01-31T20:54:28.101908625Z'
      projects/831028511762/locations/global/memberships/gke-west2-a:
        state:
          code: OK
          description: |-
            Revision(s) ready for use: asm-managed.
            All Canonical Services have been reconciled successfully.
          updateTime: '2022-01-31T14:57:16.846868685Z'
    name: projects/securegke1/locations/global/features/servicemesh
    resourceState:
      state: ACTIVE
    spec: {}
    ```

    > Run the describe command again until you see both clusters in the output as shown above.

1.  Ensure `ControlPlaneRevision` CRD is present and established in the GKE clusters.

    ```bash
    kubectl --context=${CLUSTER_1} wait --for=condition=established crd controlplanerevisions.mesh.cloud.google.com --timeout=5m
    kubectl --context=${CLUSTER_2} wait --for=condition=established crd controlplanerevisions.mesh.cloud.google.com --timeout=5m
    ```

    The output is similar to the following:

    ```
    customresourcedefinition.apiextensions.k8s.io/controlplanerevisions.mesh.cloud.google.com condition met
    ```

### Installing ASM

1.  Create `istio-system` namespace on the GKE clusters and deploy the [ControlPlaneRevision](https://cloud.google.com/service-mesh/docs/revisions-overview) CR to enable manageed ASM on the clusters.

    ```bash
    # Cluster_1
    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/secure-gke-asm-acm/asm/namespace-istio-system.yaml
    sed -e "s/ASM_CHANNEL/${ASM_CHANNEL}/" ${WORKDIR}/secure-gke-asm-acm/asm/controlplanerevision-asm-managed.yaml | kubectl --context=${CLUSTER_1} apply -f -

    # Cluster_2
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/secure-gke-asm-acm/asm/namespace-istio-system.yaml
    sed -e "s/ASM_CHANNEL/${ASM_CHANNEL}/" ${WORKDIR}/secure-gke-asm-acm/asm/controlplanerevision-asm-managed.yaml | kubectl --context=${CLUSTER_2} apply -f -
    ```

    The output is similar to the following:

    ```
    namespace/istio-system created
    controlplanerevision.mesh.cloud.google.com/asm-managed created
    ```

1.  Wait until ASM MCP is provisioned successfully.

    ```bash
    kubectl --context=${CLUSTER_1} wait --for=condition=ProvisioningFinished controlplanerevision asm-managed -n istio-system --timeout 600s
    kubectl --context=${CLUSTER_2} wait --for=condition=ProvisioningFinished controlplanerevision asm-managed -n istio-system --timeout 600s
    ```

    The output is similar to the following:

    ```
    controlplanerevision.mesh.cloud.google.com/asm-managed condition met
    ```

1.  Deploy an ASM ingress gateway on the GKE clusters.

    ```bash
    # Cluster_1
    sed -e "s/ASM_LABEL/${ASM_LABEL}/" ${WORKDIR}/secure-gke-asm-acm/asm/namespace-asm-gateways.yaml | kubectl --context=${CLUSTER_1} apply -f -
    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/secure-gke-asm-acm/asm/asm-ingressgateway.yaml

    # Cluster_2
    sed -e "s/ASM_LABEL/${ASM_LABEL}/" ${WORKDIR}/secure-gke-asm-acm/asm/namespace-asm-gateways.yaml | kubectl --context=${CLUSTER_2} apply -f -
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/secure-gke-asm-acm/asm/asm-ingressgateway.yaml
    ```

    The output is similar to the following:

    ```
    service/asm-ingressgateway created
    deployment.apps/asm-ingressgateway created
    role.rbac.authorization.k8s.io/asm-ingressgateway-sds created
    rolebinding.rbac.authorization.k8s.io/asm-ingressgateway-sds created
    ```

1.  Wait until the `asm-ingressgateway` Deployment is Ready.

    ```bash
    kubectl --context=${CLUSTER_1} -n asm-gateways wait --for=condition=available deployment asm-ingressgateway --timeout=5m
    kubectl --context=${CLUSTER_2} -n asm-gateways wait --for=condition=available deployment asm-ingressgateway --timeout=5m
    ```

    The output is similar to the following:

    ```
    deployment.apps/asm-ingressgateway condition met
    ```

### Installing ACM Config Sync and Policy Controller

1.  Create a Cloud Source Repository to be used for Config Sync and Policy Controller.

    ```bash
    gcloud --project=${PROJECT_ID} source repos create ${ACM_REPO}
    ```

    The output is similar to the following:

    ```
    Created [acm-repo].
    ```

1.  Get the repo URL.

    ```bash
    export REPO_URL=$(gcloud source repos describe ${ACM_REPO} --format='value(url)')
    echo -e "REPO_URL is ${REPO_URL}"
    ```

    The output is similar to the following:

    ```
    REPO_URL is https://source.developers.google.com/p/project_id/r/acm-repo
    ```

1.  Create a GCP service account and assign it the `source.reader` role.

    ```bash
    gcloud --project=${PROJECT_ID} iam service-accounts create ${GSA_READER} --display-name="ACM Reader SA"
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=serviceAccount:${GSA_READER}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role=roles/source.reader
    ```

1.  Configure Config Sync with Policy Controller enabled.

    ```bash
    envsubst < ${WORKDIR}/secure-gke-asm-acm/acm/apply-spec.yaml_tmpl > ${WORKDIR}/secure-gke-asm-acm/acm/apply-spec.yaml

    # Cluster_1
    gcloud beta container hub config-management apply \
    --membership=${CLUSTER_1} \
    --config=${WORKDIR}/secure-gke-asm-acm/acm/apply-spec.yaml \
    --project=${PROJECT_ID}

    # Cluster_2
    gcloud beta container hub config-management apply \
    --membership=${CLUSTER_2} \
    --config=${WORKDIR}/secure-gke-asm-acm/acm/apply-spec.yaml \
    --project=${PROJECT_ID}
    ```

    Output is similar to the folliwing:

    ```
    Waiting for Feature Config Management to be updated...done.
    ```

1.  View the status.

    ```bash
    gcloud beta container hub config-management status \
      --project=${PROJECT_ID}
    ```

    The output is similar to the following:

    ```
    Name: gke-central1-a
    Status: PENDING
    Last_Synced_Token: NA
    Sync_Branch: NA
    Last_Synced_Time:
    Policy_Controller: INSTALLED
    Hierarchy_Controller: PENDING

    Name: gke-west2-a
    Status: PENDING
    Last_Synced_Token: NA
    Sync_Branch: NA
    Last_Synced_Time:
    Policy_Controller: INSTALLED
    Hierarchy_Controller: PENDING
    ```

    > Status will remain PENDING for now until you configure the ConfigSync repo.

1.  Create an IAM policy binding between the Kubernetes service account and the Google service account. The Kubernetes service account is not created until you configure Config Sync for the first time. This binding lets the Config Sync Kubernetes service account act as the Google service account

    ```bash
    gcloud --project=${PROJECT_ID} iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[config-management-system/root-reconciler]" \
    ${GSA_READER}@${PROJECT_ID}.iam.gserviceaccount.com
    ```

### Setting up ACM Repo

1.  Configure your Git client if it is not already configured.

    ```bash
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
    ```

1.  Download the `nomos` client.

    ```bash
    gsutil cp gs://config-management-release/released/latest/linux_amd64/nomos ${WORKDIR}/nomos && chmod +x ${WORKDIR}/nomos
    export NOMOS=${WORKDIR}/nomos
    $NOMOS version
    ```

    The output is similar to the following:

    ```
    CURRENT   CLUSTER_CONTEXT_NAME   COMPONENT           VERSION
                                     <nomos CLI>         v1.10.1-rc.3
    *         gke-central1-a         config-management   v1.10.1-rc.3
              gke-west2-a            config-management   v1.10.0-rc.8
    ```

1.  Clone the ACM repo and initialize the repo with the ACM ConfigSync [folder structure](https://cloud.google.com/anthos-config-management/docs/concepts/hierarchical-repo) using `nomos` CLI.

    ```bash
    cd ${WORKDIR}
    gcloud source repos clone ${ACM_REPO} --project=${PROJECT_ID}
    cd ${ACM_REPO}
    git checkout -b main
    ${NOMOS} init
    ```

1.  In this demo you use [referenctial constraints](https://cloud.google.com/anthos-config-management/docs/how-to/creating-constraints#referential). In order for the referential constraints to work, you must deploy the `Config` CR in the `gatekeeper-system` namespace. The `Config` manifest configures Policy Controller to watch for resources in the cluster. Create an entry with `group`, `version`, and `kind` under `spec.sync.syncOnly`, with the values for each type of object you want to watch.

    ```bash
    mkdir -p ${WORKDIR}/${ACM_REPO}/namespaces/policy/gatekeeper-system
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm/namespace-gatekeeper-system.yaml ${WORKDIR}/${ACM_REPO}/namespaces/policy/gatekeeper-system/namespace.yaml
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm/config-referential-constraints.yaml ${WORKDIR}/${ACM_REPO}/namespaces/policy/gatekeeper-system/config-referential-constraints.yaml

    ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "init and enable referential constraints"
    git push -u origin main
    ```

1.  Wait until clusters are synced with ACM repo.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    Output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T16:46:32Z', '2022-02-02T16:46:40Z']
    ```

    Type CTRL_C to exit the watch command.

    > You may need to run the command a few times until you see the `SYNCED` status.

### Deploying Bank of Anthos

1.  Clone the Bank of Anthos repo.

    ```bash
    git clone https://github.com/GoogleCloudPlatform/bank-of-anthos.git ${WORKDIR}/bank-of-anthos
    ```

    > Note: Downloading Bank of Anthos in this demo from the upstream repo ensures you have the latest demo.

1.  Create namespace folders and all resources for Bank of Anthos in the ACM repo.

    ```bash
    # Prepare Bank of Anthos manifests
    for NS in frontend userservice contacts ledger-writer balance-reader transaction-history loadgenerator ledger-db accounts-db
    do
      envsubst < ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/bank-of-anthos/${NS}/namespace.yaml_tmpl > ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/bank-of-anthos/${NS}/namespace.yaml
      cp -r ${WORKDIR}/bank-of-anthos/extras/jwt/jwt-secret.yaml ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/bank-of-anthos/${NS}/jwt-secret.yaml
      cp -r ${WORKDIR}/bank-of-anthos/kubernetes-manifests/"${NS}".yaml ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/bank-of-anthos/${NS}/${NS}.yaml
    done

    # Copy to ACM repo
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/bank-of-anthos ${WORKDIR}/${ACM_REPO}/namespaces
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/asm ${WORKDIR}/${ACM_REPO}/namespaces
    # Clean up a few resources which will be used later
    rm ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/frontend/authzpolicy-allow-all-sas.yaml
    rm ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/frontend/authzpolicy-allow-loadgen.yaml
    rm ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/frontend/destinationrule-mtls-istio-mutual.yaml
    rm ${WORKDIR}/${ACM_REPO}/namespaces/asm/istio-system/peerauthentication-mtls-strict.yaml
    ```

1.  Commit to ACM repo.

    ```bash
    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "deploy bank of anthos application"
    git push -u origin main
    ```

1.  Wait until clusters are synced with ACM repo.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T17:04:15Z', '2022-02-02T17:04:26Z']
    ```

    Type CTRL_C to exit the watch command.

1.  Access the application by browsing to the ASM ingressgateway external IP address.

    ```bash
    export ASM_INGRESS_IP_CLUSTER_1=$(kubectl --context=${CLUSTER_1} -n asm-gateways get svc asm-ingressgateway -ojsonpath='{.status.loadBalancer.ingress[].ip}')
    echo -e "ASM_INGRESS_IP_CLUSTER_1 is ${ASM_INGRESS_IP_CLUSTER_1}"

    export ASM_INGRESS_IP_CLUSTER_2=$(kubectl --context=${CLUSTER_2} -n asm-gateways get svc asm-ingressgateway -ojsonpath='{.status.loadBalancer.ingress[].ip}')
    echo -e "ASM_INGRESS_IP_CLUSTER_2 is ${ASM_INGRESS_IP_CLUSTER_2}"
    ```

Demo setup is now complete. You should be able to access the Bank of Anthos application through either of the ASM ingress gateway IP addresses.

You should have the following:

1.  Two GKE clusters with ASM and ACM (with Policy Controller) installed.
1.  Bank of Anthos application deployed with all Services in their own respective namespaces using Anthos Config Management ConfigSync repo.

## Demo

### Deploying policies through Policy Controller in `dryrun` mode

Thus far, you have created a GKE cluster, installed ASM and ACM on the cluster and deployed an application (Bank of Anthos). The application is running in an unsecure manner. This means there is no encryption (or authentation) configured between the services, no [NetworkPolicies](https://kubernetes.io/docs/concepts/services-networking/network-policies/) between namespaces and no [authorization policies](https://istio.io/latest/docs/reference/config/security/authorization-policy/). Hence, all services can connect to all other services in an insecure manner. In this section you deploy policies on the GKE cluster to increase its security posture.

#### Configuring Strict mTLS

This constraint requires that ASM authentication Policy specify peers with STRICT mutual TLS. Deploy the constraint to CLUSTER_1 in `dry-run` mode. The `dry-run` mode does not enforce the policy but allows you to validate and inspect prior to applying the policy to your environment.

1.  Create the constraint and commit to ACM repo.

    ```bash
    sed -e "s/CLUSTER_1/${CLUSTER_1}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-1-mtls-strict.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-1-mtls-strict.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "constraint strict mtls dry-run on cluster 1"
    git push -u origin main
    ```

1.  Wait until clusters are synced with ACM repo.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T17:08:10Z', '2022-02-02T17:08:14Z']
    ```

    Type CTRL_C to exit the watch command.

1.  Inspect the violation.

    ```bash
    kubectl --context=${CLUSTER_1} get policystrictonly policy-strict-constraint -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    [
      {
        "enforcementAction": "dryrun",
        "kind": "PeerAuthentication",
        "message": "spec.mtls.mode must be set to `STRICT`",
        "name": "default",
        "namespace": "istio-system"
      }
    ]
    ```

Since the policy is in `dryrun` mode, it provides the violation in the resource status.

#### Configuring Destination Rule TLS

This constraint prohibits disabling TLS for all hosts and host subsets in Istio DestinationRules.

1.  Update the constraint template for the new API and deploy the constraint with the new constraint template.

    ```bash
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constrainttemplate-destinationruletlsenabledbeta.yaml ${WORKDIR}/${ACM_REPO}/cluster/constrainttemplate-destinationruletlsenabledbeta.yaml
    sed -e "s/CLUSTER_1/${CLUSTER_1}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-1-mtls-destinationrule.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-1-mtls-destinationrule.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "constraint destinationrule enabled dry-run on cluster 1"
    git push -u origin main
    ```

1.  Wait until clusters are synced with ACM repo.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T17:09:29Z', '2022-02-02T17:10:01Z']
    ```

    Type CTRL_C to exit the watch command.

    > You may see a temporary error stating that no CustomResourceDefinition is defined for the type "DestinationRuleTLSEnabledBeta.constraints.gatekeeper.sh. This error should resolve itself by waiting and re-running the status command again.

1.  Inspect the DestinationRule is the `frontend` namespace. When you deployed Bank of Anthos, you deployed a DestinationRule with mTLS disabled.

    ```bash
    kubectl --context=${CLUSTER_1} -n frontend get destinationrule destrule-mtls-disable -ojsonpath={.spec} | jq
    ```

    Output is similar to the following:

    ```
    {
      "host": "frontend",
      "trafficPolicy": {
        "loadBalancer": {
          "simple": "LEAST_CONN"
        },
        "tls": {
          "mode": "DISABLE"
        }
      }
    }
    ```

    Note that `TLS` mode is set to `DISABLE`. This should trigger a violation of the policy you just implemented.

1.  View the violation.

    ```bash
    kubectl --context=${CLUSTER_1} get destinationruletlsenabledbeta destinationrule-mtls-enabled -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    [
      {
        "enforcementAction": "dryrun",
        "kind": "DestinationRule",
        "message": "spec.trafficPolicy.tls.mode == DISABLE for host(s): frontend",
        "name": "destrule-mtls-disable",
        "namespace": "frontend"
      }
    ]
    ```

Since the policy is in `dryrun` mode, it provides the violation in the status.

#### Configuring SourceNotAllAuthz policy

This constraint requires that ASM AuthorizationPolicy rules have source principals set to something other than "\*".

1.  Create an AuthorizationPolicy which allows all principals and namespaces to access the `frontend` Service. Create the constraint that disallows using wildcards as principals and namespaces.

    > Note: After applying this AuthorizationPolicy, you will get an `RBAC: access denied` message if you try to access the Bank of Anthos application from either cluster. The reason is because in the previous step, you disabled mTLS and AuthorizationPolicies are relying on certificates to check principals.

    ```bash
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/bank-of-anthos/frontend/authzpolicy-allow-all-sas.yaml ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/frontend/authzpolicy-allow-all-sas.yaml
    sed -e "s/CLUSTER_1/${CLUSTER_1}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-1-sourcenotall-authz.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-1-sourcenotall-authz.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "constraint sourcenotall-authz enabled dry-run on cluster 1"
    git push -u origin main
    ```

1.  Wait until clusters are synced with ACM repo.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T17:12:20Z', '2022-02-02T17:12:27Z']
    ```

    Type CTRL_C to exit the watch command.

1.  Inspect the AuthorizationPolicy is the `frontend` namespace.

    ```bash
    kubectl --context=${CLUSTER_1} -n frontend get authorizationpolicy allow-all-sas -ojsonpath='{.spec}'  | jq
    ```

    Output is similar to the following:

    ```
    {
      "action": "ALLOW",
      "rules": [
        {
          "from": [
            {
              "source": {
                "principals": [
                  "*"
                ]
              }
            },
            {
              "source": {
                "namespaces": [
                  "*"
                ]
              }
            }
          ]
        }
      ]
    }
    ```

    Note that all namespaces and principals are allowed using a wildcard `*` setting in the AuthorizationPolicy. This should trigger a violation of the policy you just implemented.

1.  View the violation.

    ```bash
    kubectl --context=${CLUSTER_1} get sourcenotallauthz sourcenotall-authz -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    [
      {
        "enforcementAction": "dryrun",
        "kind": "AuthorizationPolicy",
        "message": "source.principals[] cannot be '*'",
        "name": "allow-all-sas",
        "namespace": "frontend"
      }
    ]
    ```

Since the policy is in `dryrun` mode, it provides the violation in the status.

#### Configuring K8sRequireNamespaceNetworkPolicies

This constraint requires that every namespace defined in the cluster has a NetworkPolicy.

1.  Create the constraint.

    ```bash
    sed -e "s/CLUSTER_1/${CLUSTER_1}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-1-require-networkpolicy.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-1-require-networkpolicy.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "constraint require-networkpolicies enabled dry-run on cluster 1"
    git push -u origin main
    ```

1.  Wait until clusters are synced with ACM repo.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T17:15:26Z', '2022-02-02T17:15:27Z']
    ```

    Type CTRL_C to exit the watch command.

1.  Since you have not deployed any NetworkPolicies in any namespaces, you should get violations on all nameespaces. View the violations.

    ```bash
    kubectl --context=${CLUSTER_1} get K8srequirenamespacenetworkpolicies require-namespace-network-policies -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    Excerpt from output:
    [
      {
        "enforcementAction": "dryrun",
        "kind": "Namespace",
        "message": "Namespace <accounts-db> does not have a NetworkPolicy",
        "name": "accounts-db"
      },
      {
        "enforcementAction": "dryrun",
        "kind": "Namespace",
        "message": "Namespace <asm-gateways> does not have a NetworkPolicy",
        "name": "asm-gateways"
      },
      {
        "enforcementAction": "dryrun",
        "kind": "Namespace",
        "message": "Namespace <balance-reader> does not have a NetworkPolicy",
        "name": "balance-reader"
      },
    ...
    ```

Since the policy is in `dryrun` mode, it provides the violation in the status.

### Creating a constraint template

In this section, you create a new ConstraintTemplate. This ConstraintTemplate ensures that every namespace has an Authorization Policy. This is similar to the previous constraint template.

1.  Create the constraint template.

    ```bash
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constrainttemplate-k8srequirenamespaceauthzpolicies.yaml ${WORKDIR}/${ACM_REPO}/cluster/constrainttemplate-k8srequirenamespaceauthzpolicies.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "constraint template k8srequirenamespaceauthzpolicies"
    git push -u origin main
    ```

1.  Wait until clusters are synced with ACM repo.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T17:22:47Z', '2022-02-02T17:22:56Z']
    ```

    Type CTRL_C to exit the watch command.

1.  Verify that the constraint template is deployed in the clusters and inspect the constraint template.

    ```bash
    kubectl --context=${CLUSTER_1} get constrainttemplate | grep authzpolicies
    kubectl --context=${CLUSTER_1} get constrainttemplate k8srequirenamespaceauthzpolicies -ojsonpath='{.spec.targets[].rego}'
    ```

    The output is similar to the following:

    ```
    package k8srequirenamespaceauthzpolicies

    violation[{"msg": msg}] {
      input.review.kind.kind == "Namespace"
      not namespace_has_authzpolicy(input.review.object.metadata.name)
      msg := sprintf("Namespace <%v> does not have an Authorization Policy", [input.review.object.metadata.name])
    }

    namespace_has_authzpolicy(ns) {
      ap := data.inventory.namespace[ns][_].AuthorizationPolicy[_]
    }
    ```

    The constraint template looks for the resource `AuthorizationPolicy` in every namespace and throws a violation if there is none.

1.  Create the constraint.

    ```bash
    sed -e "s/CLUSTER_1/${CLUSTER_1}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-1-k8srequirenamespaceauthzpolicies.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-1-k8srequirenamespaceauthzpolicies.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "constraint require-authzpolicies enabled dry-run on cluster 1"
    git push -u origin main
    ```

1.  Wait until clusters are synced with ACM repo.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T17:22:47Z', '2022-02-02T17:25:51Z']
    ```

    Type CTRL_C to exit the watch command.

1.  Besides the `frontend` namespace, no other namespace has an AuthorizationPolicy deployed so you should get violations on all namespaces except `frontend`. View the violations.

    ```bash
    kubectl --context=${CLUSTER_1} get K8srequirenamespaceauthzpolicies require-namespace-authz-policies -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    Excerpt from output:
    [
      {
        "enforcementAction": "dryrun",
        "kind": "Namespace",
        "message": "Namespace <accounts-db> does not have an Authorization Policy",
        "name": "accounts-db"
      },
      {
        "enforcementAction": "dryrun",
        "kind": "Namespace",
        "message": "Namespace <asm-gateways> does not have an Authorization Policy",
        "name": "asm-gateways"
      },
      {
        "enforcementAction": "dryrun",
        "kind": "Namespace",
        "message": "Namespace <balance-reader> does not have an Authorization Policy",
        "name": "balance-reader"
      },
    ...
    ```

Since the policy is in `dryrun` mode, it provides the violation in the status.

### Deploying policies through Policy Controller in `deny` mode

Deploy all of the same constraints on CLUSTER_2, but this time deploy these in `deny` enforcementMode.

1.  There are no policies currently deployed on CLUSTER_2. Create all of the same policies as you deployed on CLUSTER_1, but this time in `deny` enforcementMode.

    ```bash
    sed -e "s/CLUSTER_2/${CLUSTER_2}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-2-k8srequirenamespaceauthzpolicies.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-2-k8srequirenamespaceauthzpolicies.yaml
    sed -e "s/CLUSTER_2/${CLUSTER_2}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-2-mtls-destinationrule.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-2-mtls-destinationrule.yaml
    sed -e "s/CLUSTER_2/${CLUSTER_2}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-2-mtls-strict.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-2-mtls-strict.yaml
    sed -e "s/CLUSTER_2/${CLUSTER_2}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-2-require-networkpolicy.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-2-require-networkpolicy.yaml
    sed -e "s/CLUSTER_2/${CLUSTER_2}/" ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/constraint-gke-2-sourcenotall-authz.yaml > ${WORKDIR}/${ACM_REPO}/cluster/constraint-gke-2-sourcenotall-authz.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "add all constraints to cluster 2 in deny mode"
    git push -u origin main
    ```

1.  Wait a few moments the ConfigSync status.

    ```bash
    watch gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format="'table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'"
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['ERROR', 'SYNCED']
    LAST_SYNCED: ['NA', '2022-02-02T17:27:24Z']
    ```

    Type CTRL_C to exit the watch command.

    You should error from CLUSTER_2 now. Since all AuthorizationPolicies are now set to enforecementMode `deny`, you can see the violations in ConfigSync status.

1.  You can get the detailed errors using the `nomos` command.

    ```bash
    ${NOMOS} status --contexts ${CLUSTER_2}
    ```

    The excerpt of the output is as follows:

    ```
    *gke-central1-a
      --------------------
      <root>   https://source.developers.google.com/p/qwiklabs-gcp-04-a029f9807c40/r/acm-repo@main
      ERROR    467c5562988a2cdc9cd079d9c432f794f0f21f9b
      Error:   KNV2009: failed to apply AuthorizationPolicy.security.istio.io, frontend/allow-all-sas: admission webhook "validation.gatekeeper.sh" denied the request: [sourcenotall-authz] source.principals[] cannot be '*'

    For more information, see https://g.co/cloud/acm-errors#knv2009
      Error:                                                          KNV2009: failed to apply Namespace, /accounts-db: admission webhook "validation.gatekeeper.sh" denied the request: [require-namespace-network-policies] Namespace <accounts-db> does not have a NetworkPolicy
    [require-namespace-authz-policies] Namespace <accounts-db> does not have an Authorization Policy

    For more information, see https://g.co/cloud/acm-errors#knv2009
      Error:                                                          KNV2009: failed to apply Namespace, /balance-reader: admission webhook "validation.gatekeeper.sh" denied the request: [require-namespace-network-policies] Namespace <balance-reader> does not have a NetworkPolicy
    [require-namespace-authz-policies] Namespace <balance-reader> does not have an Authorization Policy

    For more information, see https://g.co/cloud/acm-errors#knv2009
      Error:                                                          KNV2009: failed to apply Namespace, /contacts: admission webhook "validation.gatekeeper.sh" denied the request: [require-namespace-network-policies] Namespace <contacts> does not have a NetworkPolicy
    [require-namespace-authz-policies] Namespace <contacts> does not have an Authorization Policy
    ...
    ```

### Resolving the Policy Controller violations

There are currently five policy constraints on both clusters. These are as follows:

1.  _Strict mTLS_: Achieve end-to-end encryption between all services in your cluster by requiring mesh wide strict mTLS using ASM.
1.  _Destination Rule mTLS_: Achieve enforcement of per-Service encryption by enforcing ASM Destination Rules to have have STRICT mTLS configured.
1.  _SourceNotAllAuthz_: Achieve granular access policies to every Service by requiring ASM Authorization Policies to not have wildcards as principals.
1.  _K8sRequireNamespaceNetworkPolicies_: Achieve granular traffic control between namespaces by enforcing that all namespaces must have (one or more) network policies defined.
1.  _K8sRequireNamespaceNetworkPolicies_: Achieve granular access control between Services by enforcing that every namespace has (one or more) AuthorizationPolicy resources defined.

#### Enabling end-to-end mTLS

1.  Inspect the violation.

    ```bash
    kubectl --context=${CLUSTER_2} get policystrictonly policy-strict-constraint -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    [
      {
        "enforcementAction": "deny",
        "kind": "PeerAuthentication",
        "message": "spec.mtls.mode must be set to `STRICT`",
        "name": "default",
        "namespace": "istio-system"
      }
    ]
    ```

1.  Resolve the violation by enabling `STRICT` mTLS PeerAuthentication resource in the `istio-system` namespace.

    ```bash
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/asm/istio-system/peerauthentication-mtls-strict.yaml ${WORKDIR}/${ACM_REPO}/namespaces/asm/istio-system/peerauthentication-mtls-strict.yaml
    rm ${WORKDIR}/${ACM_REPO}/namespaces/asm/istio-system/peerauthentication-mtls-disable.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "enable STRICT mTLS mesh wide"
    git push -u origin main
    ```

1.  Wait a few moments and ensure that CLUSTER_2 has STRICT mTLS enabled.

    ```bash
    kubectl --context=${CLUSTER_2} -n istio-system get peerauthentication default -ojsonpath='{.spec}' | jq
    ```

    The output is similar to the following:

    ```
    {
      "mtls": {
        "mode": "STRICT"
      }
    }
    ```

    > Mode will change from `DISABLE` to `STRICT`.

1.  Inspect the constraint for violations.

    ```bash
    kubectl --context=${CLUSTER_2} get policystrictonly policy-strict-constraint -ojsonpath='{.status.violations}'  | jq
    ```

    The output is empty. This means this constraint is no longer in violation.

#### Enforcing per Service encryption

1.  Inspect the violation.

    ```bash
    kubectl --context=${CLUSTER_2} get destinationruletlsenabledbeta destinationrule-mtls-enabled -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    [
      {
        "enforcementAction": "deny",
        "kind": "DestinationRule",
        "message": "spec.trafficPolicy.tls.mode == DISABLE for host(s): frontend",
        "name": "destrule-mtls-disable",
        "namespace": "frontend"
      }
    ]
    ```

1.  Resolve the violation by enabling `STRICT` mTLS DestinationRule resource in the `frontend` namespace.

    ```bash
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/bank-of-anthos/frontend/destinationrule-mtls-istio-mutual.yaml ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/frontend/destinationrule-mtls-istio-mutual.yaml
    rm ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/frontend/destinationrule-mtls-disabled.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "enable ISTIO_MUTUAL TLS for frontend Service"
    git push -u origin main
    ```

1.  Wait a few moments and ensure that CLUSTER_2 `frontend` namespace has a DestinationRule with ISTIO_MUTUAL TLS setting.

    ```bash
    kubectl --context=${CLUSTER_2} -n frontend get destinationrule destrule-mtls-istio-mutual -ojsonpath='{.spec}' | jq
    ```

    The output is similar to the following:

    ```
    {
      "host": "frontend",
      "trafficPolicy": {
        "loadBalancer": {
          "simple": "LEAST_CONN"
        },
        "tls": {
          "mode": "ISTIO_MUTUAL"
        }
      }
    }
    ```

    You may initially see an error stating that the DestinationRule `destrule-mtls-istio-mutual` not found. This error will resolve once the repo syncs to the cluster.

1.  Inspect the constraint for violations.

    ```bash
    kubectl --context=${CLUSTER_2} get destinationruletlsenabledbeta destinationrule-mtls-enabled -ojsonpath='{.status.violations}'  | jq
    ```

    The output is empty meaning this constraint is no longer in violation.

#### Enforcing granular access policies for Services

1.  Inspect the violation.

    ```bash
    kubectl --context=${CLUSTER_2} get sourcenotallauthz sourcenotall-authz -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    [
      {
        "enforcementAction": "deny",
        "kind": "AuthorizationPolicy",
        "message": "source.principals[] cannot be '*'",
        "name": "allow-all-sas",
        "namespace": "frontend"
      }
    ]
    ```

1.  Resolve the violation by configuring granular access policy (AuthorizationPolicy) resource in the `frontend` namespace.

    ```bash
    cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/namespaces/bank-of-anthos/frontend/authzpolicy-allow-loadgen.yaml ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/frontend/authzpolicy-allow-loadgen.yaml
    rm ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/frontend/authzpolicy-allow-all-sas.yaml

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "enable granular Authz policy for frontend Service"
    git push -u origin main
    ```

1.  Wait a few moments and ensure that CLUSTER_2 `frontend` namespace has a granular AuthorizationPolicy (which does not use wildcards for principals or namespaces).

    ```bash
    kubectl --context=${CLUSTER_2} -n frontend get authorizationpolicy allow-loadgen -ojsonpath='{.spec}' | jq
    ```

    The output is similar to the following:

    ```
    {
      "action": "ALLOW",
      "rules": [
        {
          "from": [
            {
              "source": {
                "principals": [
                  "cluster.local/ns/loadgenerator/sa/default"
                ]
              }
            },
            {
              "source": {
                "namespaces": [
                  "loadgenerator"
                ]
              }
            }
          ]
        }
      ]
    }
    ```

    You may initially see an error stating that AuthorizationPolicy `allow-loadgen` is not found. This error will resolve once the repo syncs to the cluster.

1.  Inspect the constraint for violations.

    ```bash
    kubectl --context=${CLUSTER_2} get sourcenotallauthz sourcenotall-authz -ojsonpath='{.status.violations}'  | jq
    ```

    The output is empty meaning this constraint is no longer in violation.

#### Enforcing granular traffic control between namespaces

1.  Inspect the violations.

    ```bash
    kubectl --context=${CLUSTER_2} get K8srequirenamespacenetworkpolicies require-namespace-network-policies -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    Excerpt from output:
    [
      {
        "enforcementAction": "deny",
        "kind": "Namespace",
        "message": "Namespace <accounts-db> does not have a NetworkPolicy",
        "name": "accounts-db"
      },
      {
        "enforcementAction": "deny",
        "kind": "Namespace",
        "message": "Namespace <asm-gateways> does not have a NetworkPolicy",
        "name": "asm-gateways"
      },
      {
        "enforcementAction": "deny",
        "kind": "Namespace",
        "message": "Namespace <balance-reader> does not have a NetworkPolicy",
        "name": "balance-reader"
      },
    ...
    ```

1.  Resolve the violations by configuring a NetworkPolicies in every namespace.

    ```bash
    for NS in frontend userservice contacts ledger-writer balance-reader transaction-history loadgenerator ledger-db accounts-db
    do
      cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/bank-of-anthos-network-policy-ingress.yaml ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/${NS}/bank-of-anthos-network-policy-ingress.yaml
      cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/bank-of-anthos-network-policy-allow-egress.yaml ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/${NS}/bank-of-anthos-network-policy-allow-egress.yaml
    done

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "configure network policies in every namespace"
    git push -u origin main
    ```

1.  Wait a few moments and ensure that every namespace in CLUSTER_2 has NetworkPolicy resources.

    ```bash
    for NS in frontend userservice contacts ledger-writer balance-reader transaction-history loadgenerator ledger-db accounts-db
    do
      kubectl --context=${CLUSTER_2} -n ${NS} get networkpolicy
    done
    ```

    The output is similar to the following:

    ```
    # For all nameaspaces
    NAME               POD-SELECTOR   AGE
    allow-all-egress   <none>         49s
    allow-ingress      <none>         49s
    ...
    ```

    You may initially see errors stating that NetworkPolicies do not exist in certain namespaces. These errors should will resolve once the repo syncs to the cluster.

1.  Inspect the constraint for violations.

    ```bash
    kubectl --context=${CLUSTER_2} get K8srequirenamespacenetworkpolicies require-namespace-network-policies -ojsonpath='{.status.violations}'  | jq
    ```

    The output is empty meaning this constraint is no longer in violation.

#### Enforcing granular access control between Services

1.  Inspect the violations.

    ```bash
    kubectl --context=${CLUSTER_2} get K8srequirenamespaceauthzpolicies require-namespace-authz-policies -ojsonpath='{.status.violations}'  | jq
    ```

    The output is similar to the following:

    ```
    Excerpt from output:
    [
      {
        "enforcementAction": "deny",
        "kind": "Namespace",
        "message": "Namespace <accounts-db> does not have an Authorization Policy",
        "name": "accounts-db"
      },
      {
        "enforcementAction": "deny",
        "kind": "Namespace",
        "message": "Namespace <asm-gateways> does not have an Authorization Policy",
        "name": "asm-gateways"
      },
      {
        "enforcementAction": "deny",
        "kind": "Namespace",
        "message": "Namespace <balance-reader> does not have an Authorization Policy",
        "name": "balance-reader"
      },
    ...
    ```

1.  Resolve the violations by creating AnthorizationPolicy resources in every namespace.

    ```bash
    for NS in frontend userservice contacts ledger-writer balance-reader transaction-history loadgenerator ledger-db accounts-db
    do
      cp -r ${WORKDIR}/secure-gke-asm-acm/acm-repo/cluster/bank-of-anthos-authz-policy.yaml ${WORKDIR}/${ACM_REPO}/namespaces/bank-of-anthos/${NS}/bank-of-anthos-authz-policy.yaml
    done

    cd ${WORKDIR}/${ACM_REPO}
    git add . && git commit -am "configure authz policies in every namespace"
    git push -u origin main
    ```

1.  Wait a few moments and ensure that every namespace in CLUSTER_2 has AuthorizationPolicy resources.

    ```bash
    for NS in frontend userservice contacts ledger-writer balance-reader transaction-history loadgenerator ledger-db accounts-db
    do
      kubectl --context=${CLUSTER_2} -n ${NS} get authorizationpolicy
    done
    ```

    The output is similar to the following:

    ```
    # For all nameaspaces
    NAME                AGE
    bank-authz-policy   28s
    ...
    ```

    You may initially see errors stating that AuthorizationPolicies do not exist in certain namespaces. These errors should will resolve once the repo syncs to the cluster.

1.  Inspect the constraint for violations.

    ```bash
    kubectl --context=${CLUSTER_2} get K8srequirenamespaceauthzpolicies require-namespace-authz-policies -ojsonpath='{.status.violations}'  | jq
    ```

    The output is empty meaning this constraint is no longer in violation.

1.  Check the ConfigSync status.

    ```bash
    gcloud beta container hub config-management status \
      --project=${PROJECT_ID} --format='table(acm_status.name,acm_status.config_sync,acm_status.last_synced)'
    ```

    The output is similar to the following:

    ```
    NAME: ['gke-central1-a', 'gke-west2-a']
    CONFIG_SYNC: ['SYNCED', 'SYNCED']
    LAST_SYNCED: ['2022-02-02T19:11:10Z', '2022-02-02T19:11:02Z']
    ```

Congratulations! You have successfully created a secure GKE environment. Every namespace and service in your environment is protected by the following:

1.  _NetworkPolicy_ - To ensure only required namespaces and services can communicate with each other. This is an L4 policy.
1.  _AuthorizationPolicy_ - To ensure only required principals (service accounts) can communicate with each other. This is an L7 policy.
1.  _Authentication_ - All service to service communication is encrypted via STRICT mTLS.
